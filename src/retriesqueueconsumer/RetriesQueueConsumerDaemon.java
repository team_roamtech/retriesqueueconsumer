/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retriesqueueconsumer;

import db.MySQL;
import java.sql.SQLException;
import java.util.Timer;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import utils.Logging;
import utils.Props;

/**
 *
 * @author dennis
 */
public class RetriesQueueConsumerDaemon implements Daemon, Runnable {

    /**
     * The worker thread that does all the work.
     */
    private transient Thread worker;
    /**
     * Flag to check if the worker thread should processRequests.
     */
    private transient boolean working = false;
    /**
     * Logger for this application.
     */
    private transient Logging log;
    /**
     * Loads system properties.
     */
    private transient Props props;
    /**
     * The main processRequests class.
     */

    //ExecutorService consumerExecutor;
    private transient RetriesQueueConsumer retriesQueueConsumer;

    private transient MySQL mysql;
    private transient MySQL mysqlEmal3;

    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
        worker = new Thread(this);

        props = new Props();

        log = new Logging(props);
        log.info("Initializing MD daemon...");

        try {

            mysql = new MySQL(props.getDbHost(), props.getDbPort(),
                    props.getDbName(), props.getDbUserName(),
                    props.getDbPassword(), props.getDbPoolName(),
                    props.getMaxConnections());

            mysqlEmal3 = new MySQL(props.getEmal3DbHost(), props.getEmal3DbPort(),
                    props.getEmal3DbName(), props.getEmali3DbUserName(),
                    props.getEmal3DbPassword(), props.getEmali3DbPoolName(),
                    props.getMaxConnections());

        } catch (ClassNotFoundException ex) {
            log.fatal(ex.getMessage());
        } catch (InstantiationException ex) {
            log.fatal(ex.getMessage());
        } catch (IllegalAccessException ex) {
            log.fatal(ex.getMessage());
        } catch (SQLException ex) {
            log.fatal(ex.getMessage());
        }

        retriesQueueConsumer = new RetriesQueueConsumer(props, log, mysql, mysqlEmal3);

    }

    @Override
    public void start() throws Exception {
        working = true;
        worker.start();
        log.info("Starting Retries daemon...");

    }

    @Override
    public void stop() throws Exception {
        log.info("Stopping RetriesDaemon Retries Processor daemon...");
        working = false;
        if (!RetriesQueueConsumer.isCurrentPoolShutDown) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                log.error("InterruptedException occured while waiting for "
                        + "tasks to complete: " + ex.getMessage());
            }
        }
    }

    @Override
    public void destroy() {
        log.info("Destroying Retries daemon...");
        log.info("Exiting...");
    }

    /**
     * Runs the thread. The application runs inside an "infinite" loop.
     */
    @Override
    @SuppressWarnings({"SleepWhileHoldingLock", "SleepWhileInLoop"})
    public void run() {
        while (working) {
            try {
                log.info("starting processRequests ... ");
                retriesQueueConsumer.processQueue();
                Thread.sleep(props.getSleepTime());
            } catch (InterruptedException ex) {
                log.error("InterruptedException occured: " + ex.getMessage());
            } catch (Exception ex) {
                log.error("General error occured: " + ex.getMessage());
            }

        }
    }

}
