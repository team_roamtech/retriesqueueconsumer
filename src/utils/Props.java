package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * Loads system properties from a file.
 *
 * @author Reuben Paul wafula
 */
@SuppressWarnings({"FinalClass", "ClassWithoutLogger"})
public final class Props {

    /**
     * The properties file.
     */
    private static final String PROPS_FILE = "conf/Retries.conf";

    IniParser dlrParser;
    /**
     * A list of any errors that occurred while loading the properties.
     */
    private List<String> loadErrors;
    /**
     * Info log level. Default = INFO.
     */
    private String infoLogLevel = "INFO";
    /**
     * Info log level. Default = INFO.
     */
    private String timerLogLevel = "TIMER";
    /**
     * Error log level. Default = ERROR.
     */
    private String errorLogLevel = "ERROR";
    /**
     * Fatal log level. Default = FATAL.
     */
    private String fatalLogLevel = "FATAL";
    /**
     * Error log file name.
     */
    private String infoLogFile;
    /**
     * Timer log file name.
     */
    private String timerLogFile;
    /**
     * Error log file name.
     */
    private String errorLogFile;
    /**
     * Fatal log file name.
     */
    private String fatalLogFile;

    //Rabbit configs
    private String rabbitHost;
    private String rabbitPort;
    private String rabbitUsername;
    private String rabbitPassword;
    private String rabbitVhost;

    private String[] consumerQueueNames;
    private String[] consumerQueueTypes;
    private String[] consumerDurability;
    private String[] consumerUrls;
    private String[] consumerExchange;
    private String[] consumerExchangeDurability;
    private int numOfChannelsPerQueue;
    private String[] routingKeys;
    private String dlrEndpointConfigFile = "conf/dlr_endpoint.ini";

    /**
     * Fetch bucket size.
     */
    private int bucketSize;

    /**
     * The max queue size.
     */
    private int maxQueueSize;
    /**
     * Daemon sleep time.
     */
    private int sleepTime = 1000;
    /**
     * Number of threads created per queue
     */
    private int numOfThreads;

    private String sdpUrl;

    /**
     * Database connection pool name.
     */
    private transient String dbPoolName;
    private transient String emali3DbPoolName;
    /**
     * Database user name.
     */
    private transient String dbUserName;
    private transient String emali3DbUserName;
    /**
     * Database password.
     */
    private transient String dbPassword;
    private transient String emal3DbPassword;
    /**
     * Database host.
     */
    private transient String dbHost;
    private transient String emal3DbHost;
    /**
     * Database port.
     */
    private transient String dbPort;
    private transient String emal3DbPort;
    /**
     * Database name.
     */
    private transient String dbName;
    private transient String emal3DbName;

    private transient int maxConnections;

    private HashMap<String, Properties> dlrEndPoints;

    /**
     * Constructor.
     */
    public Props() {
        loadErrors = new ArrayList<String>(0);
        loadProperties(PROPS_FILE);
    }

    /**
     * Load system properties.
     *
     * @param propsFile the system properties xml file
     */
    @SuppressWarnings({"UseOfSystemOutOrSystemErr", "unchecked"})
    private void loadProperties(final String propsFile) {
        FileInputStream propsStream = null;
        Properties props;

        try {
            props = new Properties();
            File base = new File(
                    //System.getProperty("user.dir")
                    Props.class.getProtectionDomain().getCodeSource().getLocation().toURI()
            ).getParentFile();

            propsStream = new FileInputStream(new File(base, propsFile));
            props.load(propsStream);

            String error1 = "ERROR: %s is <= 0 or may not have been set";
            String error2 = "ERROR: %s may not have been set";

            // Extract the values from the configuration file
            timerLogLevel = props.getProperty("TimerLogLevel");
            if (infoLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "TimerLogLevel"));
            }

            infoLogLevel = props.getProperty("InfoLogLevel");
            if (infoLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogLevel"));
            }

            errorLogLevel = props.getProperty("ErrorLogLevel");
            if (errorLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogLevel"));
            }

            fatalLogLevel = props.getProperty("FatalLogLevel");
            if (fatalLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogLevel"));
            }

            infoLogFile = props.getProperty("InfoLogFile");
            if (infoLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogFile"));
            }

            timerLogFile = props.getProperty("TimerLogFile");
            if (timerLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "TimerLogFile"));
            }

            errorLogFile = props.getProperty("ErrorLogFile");
            if (errorLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogFile"));
            }

            fatalLogFile = props.getProperty("FatalLogFile");
            if (fatalLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogFile"));
            }

            dbPoolName = props.getProperty("DbPoolName");
            if (dbPoolName.isEmpty()) {
                loadErrors.add(String.format(error2, "DbPoolName"));
            }

            setEmali3DbPoolName(props.getProperty("Emali3DbPoolName"));
            if (getEmali3DbPoolName().isEmpty()) {
                loadErrors.add(String.format(error2, "Emali3DbPoolName"));
            }

            setDbUserName(props.getProperty("DbUserName"));
            if (getDbUserName().isEmpty()) {
                loadErrors.add(String.format(error2, "DbUserName"));
            }

            setEmali3DbUserName(props.getProperty("Emali3DbUserName"));
            if (getEmali3DbUserName().isEmpty()) {
                loadErrors.add(String.format(error2, "Emali3DbUserName"));
            }

            setDbPassword(props.getProperty("DbPassword"));
            if (getDbPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "DbPassword"));
            }

            setEmal3DbPassword(props.getProperty("EmaliDbPassword"));
            if (getEmal3DbPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "EmaliDbPassword"));
            }

            setDbHost(props.getProperty("DbHost"));
            if (getDbHost().isEmpty()) {
                loadErrors.add(String.format(error2, "DbHost"));
            }
            setEmal3DbHost(props.getProperty("EmaliDbHost"));
            if (getEmal3DbHost().isEmpty()) {
                loadErrors.add(String.format(error2, "EmaliDbHost"));
            }

            setDbPort(props.getProperty("DbPort"));
            if (getDbPort().isEmpty()) {
                loadErrors.add(String.format(error2, "DbPort"));
            }
            setEmal3DbPort(props.getProperty("Emali3DbPort"));
            if (getEmal3DbPort().isEmpty()) {
                loadErrors.add(String.format(error2, "Emali3DbPort"));
            }

            setDbName(props.getProperty("DbName"));
            if (getDbName().isEmpty()) {
                loadErrors.add(String.format(error2, "DbName"));
            }

            setEmal3DbName(props.getProperty("Emali3DbName"));
            if (getEmal3DbName().isEmpty()) {
                loadErrors.add(String.format(error2, "Emali3DbName"));
            }

            sdpUrl = props.getProperty("SDP_URL");
            System.err.println("NEW SDP " + sdpUrl);
            if (sdpUrl.isEmpty()) {
                loadErrors.add(String.format(error2, "SDP URL"));
            }

            dlrEndpointConfigFile = props.getProperty("DLR_ENDPOINT_CONFIG");
            System.err.println("NEW " + dlrEndpointConfigFile);
            if (dlrEndpointConfigFile.isEmpty()) {
                loadErrors.add(String.format(error2, "DLR_ENDPOINT_CONFIG"));
            }

            setRabbitHost(props.getProperty("RabbitHost"));
            if (getRabbitHost().isEmpty()) {
                loadErrors.add(String.format(error2, "RabbitHost"));
            }
            setRabbitPassword(props.getProperty("RabbitPassword"));
            if (getRabbitPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "RabbitPassword"));
            }

            setRabbitVhost(props.getProperty("RabbitVhost"));
            if (getRabbitVhost().isEmpty()) {
                loadErrors.add(String.format(error2, "RabbitVhost"));
            }

            setRabbitPort(props.getProperty("RabbitPort"));
            if (getRabbitPort().isEmpty()) {
                loadErrors.add(String.format(error2, "RabbitPort"));
            } else {
                try {
                    Integer.parseInt(getRabbitPort());
                } catch (NumberFormatException nfe) {
                    loadErrors.add(String.format(error2, "RabbitPort"));
                }
            }
            String maxConns = props.getProperty("MaximumConnections");

            if (maxConns.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MaximumConnections"));
            } else {
                setMaxConnections(Integer.parseInt(maxConns));
                if (getMaxConnections() <= 0) {
                    loadErrors.add(String.format(error1,
                            "MaximumConnections"));
                }
            }

            setRabbitUsername(props.getProperty("RabbitUsername"));
            if (getRabbitUsername()
                    .isEmpty()) {
                loadErrors.add(String.format(error2, "RabbitUsername"));
            }

            String maxQueue = props.getProperty("QueueThreshold");

            if (maxQueue.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "QueueThreshold"));
            } else {
                setMaxQueueSize(Integer.parseInt(maxQueue));
                if (getMaxQueueSize() < 0) {
                    loadErrors.add(String.format(error1,
                            "QueueThreshold"));
                }
            }

            String bucket = props.getProperty("BucketSize");

            if (bucket.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "BucketSize"));
            } else {
                setBucketSize(Integer.parseInt(bucket));
                if (getBucketSize() < 0) {
                    loadErrors.add(String.format(error1,
                            "BucketSize"));
                }
            }

            String channelsPerQueue = props.getProperty("ChannelsPerQueue");

            if (channelsPerQueue.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "ChannelsPerQueue"));
            } else {
                setNumOfChannelsPerQueue(Integer.parseInt(channelsPerQueue));
                if (getNumOfChannelsPerQueue() < 0) {
                    loadErrors.add(String.format(error1,
                            "ChannelsPerQueue"));
                }
            }

            String sleep = props.getProperty("SleepTime");

            if (sleep.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "SleepTime"));
            } else {
                setSleepTime(Integer.parseInt(sleep));
                if (getSleepTime() < 0) {
                    loadErrors.add(String.format(error1,
                            "SleepTime"));
                }
            }

            String queues = props.getProperty("ConsumerQueue");

            System.out.println(
                    "queues" + queues);
            if (queues.isEmpty()) {
                loadErrors.add(String.format(error1, "ConsumerQueue"));
            } else {
                //System.err.println("QUEUES @" + queues);
                setConsumerQueueNames(queues.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String routes = props.getProperty("Routes");

            System.out.println(
                    "routes" + routes);
            if (routes.isEmpty()) {
                loadErrors.add(String.format(error1, "Routes"));
            } else {
                //System.err.println("QUEUES @" + queues);
                setRoutingKeys(routes.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String queuesTypes = props.getProperty("QueueType");

            System.out.println(
                    "queuesTypes: " + queuesTypes);
            if (queuesTypes.isEmpty()) {
                loadErrors.add(String.format(error1, "QueueType"));
            } else {
                //System.err.println("QUEUES @" + queues);
                setConsumerQueueTypes(queuesTypes.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String queuesExchanges = props.getProperty("Exchange");

            System.out.println(
                    "queuesExchanges" + queuesExchanges);
            if (queuesExchanges.isEmpty()) {
                loadErrors.add(String.format(error1, "exchange"));
            } else {
                //System.err.println("QUEUES @" + queues);                
                setConsumerExchange(queuesExchanges.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String queuesDurability = props.getProperty("QueueDurability");

            System.out.println(
                    "QueueDurability" + queuesDurability);
            if (queuesDurability.isEmpty()) {
                loadErrors.add(String.format(error1, "QueueDurability"));
            } else {
                //System.err.println("QUEUES @" + queues);                
                setConsumerDurability(queuesDurability.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String exchangeDurability = props.getProperty("ExchangeDurability");

            System.out.println(
                    "ExchangeDurability" + exchangeDurability);
            if (exchangeDurability.isEmpty()) {
                loadErrors.add(String.format(error1, "ExchangeDurability"));
            } else {
                //System.err.println("QUEUES @" + queues);                
                setConsumerExchangeDurability(exchangeDurability.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String apiUrls = props.getProperty("APIurl");

            System.out.println(
                    "apiUrls" + apiUrls);
            if (apiUrls.isEmpty()) {
                loadErrors.add(String.format(error1, "APIurl"));
            } else {
                setConsumerUrls(apiUrls.replaceAll("\\s*,\\s*", ",").split(","));
            }

            String numThreads = props.getProperty("NumberOfThreads");

            System.out.println(
                    "NumberOfThreads " + numThreads);
            if (apiUrls.isEmpty()) {
                loadErrors.add(String.format(error1, "NumberOfThreads"));
            } else {
                setNumOfThreads(Integer.parseInt(numThreads));
            }

            propsStream.close();
        } catch (NumberFormatException ne) {
            System.err.println("Exiting. String value found, Integer is "
                    + "required: " + ne.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch (FileNotFoundException | URISyntaxException ne) {
            System.err.println("Exiting. Could not find the properties file: "
                    + ne.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch (IOException ioe) {
            System.err.println("Exiting. Failed to load system properties: "
                    + ioe.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        }

        //Load DLR endpoints
        try {
            dlrParser = new IniParser(getDlrEndpointConfigFile());

        } catch (Exception ex) {
            System.err.println("Failed to load dlr_end_point props file file: "
                    + ex.getMessage());
        }

    }

    public String getSPPassword(String spID) {

        return dlrParser.getString("SP_PASSWORD", spID);
    }

    public String getDLRendpoint(String service_id_shortcode) {

        return dlrParser.getString("DLR_ENDPOINT", service_id_shortcode);
    }

    public String getMysqlPoolName(String service_id_shortcode) {

        return dlrParser.getString("MYSQL_POOL", service_id_shortcode);
    }

    /**
     * A list of any errors that occurred while loading the properties.
     *
     * @return the loadErrors
     */
    public List<String> getLoadErrors() {
        return Collections.unmodifiableList(loadErrors);
    }

    /**
     * Info log level. Default = INFO.
     *
     * @return the infoLogLevel
     */
    public String getInfoLogLevel() {
        return infoLogLevel;
    }

    /**
     * Info log level. Default = INFO.
     *
     * @return the infoLogLevel
     */
    public String getTimerLogLevel() {
        return timerLogLevel;
    }

    /**
     * Error log level. Default = ERROR.
     *
     * @return the errorLogLevel
     */
    public String getErrorLogLevel() {
        return errorLogLevel;
    }

    /**
     * Fatal log level. Default = FATAL.
     *
     * @return the fatalLogLevel
     */
    public String getFatalLogLevel() {
        return fatalLogLevel;
    }

    /**
     * Error log file name.
     *
     * @return the infoLogFile
     */
    public String getInfoLogFile() {
        return infoLogFile;
    }

    /**
     * Error log file name.
     *
     * @return the timerLogFile
     */
    public String getTimerLogFile() {
        return timerLogFile;
    }

    /**
     * Error log file name.
     *
     * @return the errorLogFile
     */
    public String getErrorLogFile() {
        return errorLogFile;
    }

    /**
     * Fatal log file name.
     *
     * @return the fatalLogFile
     */
    public String getFatalLogFile() {
        return fatalLogFile;
    }

    /**
     * Gets the sleep time.
     *
     * @return the sleep time
     */
    public int getSleepTime() {
        return sleepTime;
    }

    /**
     * @param sleepTime the sleepTime to set
     */
    public void setSleepTime(int sleepTime) {
        this.sleepTime = sleepTime;
    }

    /**
     * Gets the max queue size.
     *
     * @return the max queue size
     */
    public int getMaxQueueSize() {
        return maxQueueSize;
    }

    /**
     * Gets the bucket size.
     *
     * @return the bucket size
     */
    public int getBucketSize() {
        return bucketSize;
    }

    /**
     * @param maxQueueSize the maxQueueSize to set
     */
    public void setMaxQueueSize(int maxQueueSize) {
        this.maxQueueSize = maxQueueSize;
    }

    /**
     * @param bucketSize the bucketSize to set
     */
    public void setBucketSize(int bucketSize) {
        this.bucketSize = bucketSize;
    }

    /**
     * @return the rabbitHost
     */
    public String getRabbitHost() {
        return rabbitHost;
    }

    /**
     * @param rabbitHost the rabbitHost to set
     */
    public void setRabbitHost(String rabbitHost) {
        this.rabbitHost = rabbitHost;
    }

    /**
     * @return the rabbitPort
     */
    public String getRabbitPort() {
        return rabbitPort;
    }

    /**
     * @param rabbitPort the rabbitPort to set
     */
    public void setRabbitPort(String rabbitPort) {
        this.rabbitPort = rabbitPort;
    }

    /**
     * @return the rabbitUsername
     */
    public String getRabbitUsername() {
        return rabbitUsername;
    }

    /**
     * @param rabbitUsername the rabbitUsername to set
     */
    public void setRabbitUsername(String rabbitUsername) {
        this.rabbitUsername = rabbitUsername;
    }

    /**
     * @return the rabbitPassword
     */
    public String getRabbitPassword() {
        return rabbitPassword;
    }

    /**
     * @param rabbitPassword the rabbitPassword to set
     */
    public void setRabbitPassword(String rabbitPassword) {
        this.rabbitPassword = rabbitPassword;
    }

    /**
     * @return the rabbitPath
     */
    public String getRabbitVhost() {
        return rabbitVhost;
    }

    /**
     * @param rabbitPath the rabbitPath to set
     */
    public void setRabbitVhost(String rabbitVhost) {
        this.rabbitVhost = rabbitVhost;
    }

    /**
     * @return the getQueueNames
     */
    public String[] getConsumerQueueNames() {
        return consumerQueueNames;
    }

    /**
     * @param consumerQueueNames
     * @param getQueueNames the getQueueNames to set
     */
    public void setConsumerQueueNames(String[] consumerQueueNames) {
        this.consumerQueueNames = consumerQueueNames;
    }

    /**
     * @return the channelsPerQueue
     */
    public int getNumOfChannelsPerQueue() {
        return numOfChannelsPerQueue;
    }

    /**
     * @param channelsPerQueue the channelsPerQueue to set
     */
    public void setNumOfChannelsPerQueue(int channelsPerQueue) {
        this.numOfChannelsPerQueue = channelsPerQueue;
    }

    /**
     * @return the consumerQueueTypes
     */
    public String[] getConsumerQueueTypes() {
        return consumerQueueTypes;
    }

    /**
     * @param consumerQueueTypes the consumerQueueTypes to set
     */
    public void setConsumerQueueTypes(String[] consumerQueueTypes) {
        this.consumerQueueTypes = consumerQueueTypes;
    }

    /**
     * @return the consumerUrls
     */
    public String[] getConsumerUrls() {
        return consumerUrls;
    }

    /**
     * @param consumerUrls the consumerUrls to set
     */
    public void setConsumerUrls(String[] consumerUrls) {
        this.consumerUrls = consumerUrls;
    }

    /**
     * @return the consumerExchange
     */
    public String[] getConsumerExchange() {
        return consumerExchange;
    }

    /**
     * @param consumerExchange the consumerExchange to set
     */
    public void setConsumerExchange(String[] consumerExchange) {
        this.consumerExchange = consumerExchange;
    }

    /**
     * @return the consumerDurability
     */
    public String[] getConsumerDurability() {
        return consumerDurability;
    }

    /**
     * @param consumerDurability the consumerDurability to set
     */
    public void setConsumerDurability(String[] consumerDurability) {
        this.consumerDurability = consumerDurability;
    }

    /**
     * @return the consumerExchangeDurability
     */
    public String[] getConsumerExchangeDurability() {
        return consumerExchangeDurability;
    }

    /**
     * @param consumerExchangeDurability the consumerExchangeDurability to set
     */
    public void setConsumerExchangeDurability(String[] consumerExchangeDurability) {
        this.consumerExchangeDurability = consumerExchangeDurability;
    }

    /**
     * @return the numOfThreads
     */
    public int getNumOfThreads() {
        return numOfThreads;
    }

    /**
     * @param numOfThreads the numOfThreads to set
     */
    public void setNumOfThreads(int numOfThreads) {
        this.numOfThreads = numOfThreads;
    }

    /**
     * @return the sdpUrl
     */
    public String getSdpUrl() {
        return sdpUrl;
    }

    /**
     * @param sdpUrl the sdpUrl to set
     */
    public void setSdpUrl(String sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    /**
     * @return the routingKeys
     */
    public String[] getRoutingKeys() {
        return routingKeys;
    }

    /**
     * @param routingKeys the routingKeys to set
     */
    public void setRoutingKeys(String[] routingKeys) {
        this.routingKeys = routingKeys;
    }

    /**
     * @return the dlrEndpointConfigFile
     */
    public String getDlrEndpointConfigFile() {
        return dlrEndpointConfigFile;
    }

    /**
     * @param dlrEndpointConfigFile the dlrEndpointConfigFile to set
     */
    public void setDlrEndpointConfigFile(String dlrEndpointConfigFile) {
        this.dlrEndpointConfigFile = dlrEndpointConfigFile;
    }

    /**
     * @return the dbPoolName
     */
    public String getDbPoolName() {
        return dbPoolName;
    }

    /**
     * @param dbPoolName the dbPoolName to set
     */
    public void setDbPoolName(String dbPoolName) {
        this.dbPoolName = dbPoolName;
    }

    /**
     * @return the emali3DbPoolName
     */
    public String getEmali3DbPoolName() {
        return emali3DbPoolName;
    }

    /**
     * @param emali3DbPoolName the emali3DbPoolName to set
     */
    public void setEmali3DbPoolName(String emali3DbPoolName) {
        this.emali3DbPoolName = emali3DbPoolName;
    }

    /**
     * @return the dbUserName
     */
    public String getDbUserName() {
        return dbUserName;
    }

    /**
     * @param dbUserName the dbUserName to set
     */
    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    /**
     * @return the emali3DbUserName
     */
    public String getEmali3DbUserName() {
        return emali3DbUserName;
    }

    /**
     * @param emali3DbUserName the emali3DbUserName to set
     */
    public void setEmali3DbUserName(String emali3DbUserName) {
        this.emali3DbUserName = emali3DbUserName;
    }

    /**
     * @return the dbPassword
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * @param dbPassword the dbPassword to set
     */
    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    /**
     * @return the emal3DbPassword
     */
    public String getEmal3DbPassword() {
        return emal3DbPassword;
    }

    /**
     * @param emal3DbPassword the emal3DbPassword to set
     */
    public void setEmal3DbPassword(String emal3DbPassword) {
        this.emal3DbPassword = emal3DbPassword;
    }

    /**
     * @return the dbHost
     */
    public String getDbHost() {
        return dbHost;
    }

    /**
     * @param dbHost the dbHost to set
     */
    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    /**
     * @return the emal3DbHost
     */
    public String getEmal3DbHost() {
        return emal3DbHost;
    }

    /**
     * @param emal3DbHost the emal3DbHost to set
     */
    public void setEmal3DbHost(String emal3DbHost) {
        this.emal3DbHost = emal3DbHost;
    }

    /**
     * @return the dbPort
     */
    public String getDbPort() {
        return dbPort;
    }

    /**
     * @param dbPort the dbPort to set
     */
    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    /**
     * @return the emal3DbPort
     */
    public String getEmal3DbPort() {
        return emal3DbPort;
    }

    /**
     * @param emal3DbPort the emal3DbPort to set
     */
    public void setEmal3DbPort(String emal3DbPort) {
        this.emal3DbPort = emal3DbPort;
    }

    /**
     * @return the dbName
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * @param dbName the dbName to set
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * @return the emal3DbName
     */
    public String getEmal3DbName() {
        return emal3DbName;
    }

    /**
     * @param emal3DbName the emal3DbName to set
     */
    public void setEmal3DbName(String emal3DbName) {
        this.emal3DbName = emal3DbName;
    }

    /**
     * @return the maxConnections
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * @param maxConnections the maxConnections to set
     */
    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

}
