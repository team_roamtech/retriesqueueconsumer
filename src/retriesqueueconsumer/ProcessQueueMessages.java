/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retriesqueueconsumer;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import db.MySQL;
import http.RestClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
import queue.MessageQueueEndPoint;
import utils.Logging;
import utils.Props;

/**
 *
 * @author dennis
 */
public class ProcessQueueMessages implements Runnable {

    transient RestClient restClient;
    transient Logging logger;
    transient Props props;
    transient JSONObject jsonData;
    private final transient Channel channel;
    private final transient long deliveryTag;
    private final String sdpURL;
    final MySQL emali5pool, emali3pool;

    public ProcessQueueMessages(Logging logging,
            JSONObject jsonData, Channel channel, long delivertTag,
            Props props, MySQL emali5pool, MySQL emali3pool) {
        this.logger = logging;
        this.jsonData = jsonData;
        this.channel = channel;
        this.deliveryTag = delivertTag;
        this.props = props;
        this.emali3pool = emali3pool;
        this.emali5pool = emali5pool;
        this.sdpURL = props.getSdpUrl();
        this.restClient = new RestClient(this.logger);
        logging.info("Loaded Process message Queue messages ...");

    }

    private void ack() {
        try {
            logger.info("Calling message ACK ... message process OK");
            //channel.basicReject(deliveryTag, true);
            channel.basicAck(deliveryTag, false);
        } catch (IOException exception) {
            logger.error("Consume message failed basic ack :" + exception.getLocalizedMessage(), exception);
        }
    }

    private void reject() {
        try {
            logger.info("Calling message basic reject on call failed");
            channel.basicReject(deliveryTag, true);
        } catch (IOException exception) {
            logger.error("Consume message failed basic ack :", exception);
        }
    }

    /**
     *
     * @param jsonData jsonData
     * {"requestIdentifier":"100002200301160607151739913011","msisdn":"254722904244",
     * "serviceId":"6015132000112536","shortcode":"60040",
     * "correlator":"14341_254722904244", "password":"F1vu#Wj4h",
     * "spid":"601513"}
     * @return
     */
    private Integer processDlr(JSONObject jsonData) throws NullPointerException {
        logger.info("Process DLR DATA: " + jsonData.toString());
        String spid = jsonData.optString("spid", null);

        if (spid == null) {
            throw new NullPointerException("Params not well defined");
        }
        String spPassword = props.getSPPassword(jsonData.getString("spid"));
        logger.info("Pass From props file: SPID "
                + jsonData.getString("spid") + ", PASS" + String.valueOf(spPassword));

        spPassword = (spPassword == null) ? jsonData.getString("password") : spPassword;

        String response = this.restClient.getSMSDeliveryStatus(
                jsonData.getString("msisdn"), jsonData.getString("serviceId"),
                jsonData.getString("shortcode"),
                jsonData.getString("requestIdentifier"), jsonData.getString("spid"),
                spPassword, sdpURL);

        if (response == null) {
            logger.error("Invalid response from SDP returning message to Q NACK ");
            this.reject();
            return -1;
        }

        if (response.equalsIgnoreCase("overdue")) {
            logger.error("Invalid response from SDP: Message status is overdue, Removing message from Q");
            this.ack();
            return -1;
        }
        JSONObject responseData = RestClient.parserDLRResponse(response);
        if (responseData == null) {
            logger.info("NULL response ... NACK");
            if (jsonData.getString("spid").equals("601385")) {
                logger.info("Bulk Message Removing from Q :::):" + jsonData.toString());
                this.ack();
                return -1;
            }
            this.reject();

            return -1;
        }

        /**
         * {u'queue.QMessage': {u'queueType': None, u'text': None,
         * u'correlator': None, u'networkId': None, u'network': None,
         * u'shortcodeId': u'', u'priority': 1, u'refNo': u'None_254771961417',
         * u'linkId': None, u'status': u'NACK/0x0000000a/Invalid Source
         * Address', u'msisdn': u'254771961417', u'outboxId': None,
         * u'shortCode': None, u'sdpServiceId': None, u'alertId': u'160268046',
         * u'created': None, u'mo': None, u'modified': None, u'mt': None,
         * u'sdpId': None, u'publishSequenceNumber': 0, u'profileId': None}}
         *
         */
        logger.info("SANE RESPONSE FOUND :" + responseData.toString());

        String outboxId = "0";
        //JSONObject outputData = new JSONObject();

        try {

            String correlator = jsonData.getString("correlator");
            String correlatorid[] = correlator.split("_");

            if (correlatorid.length > 1) {
                outboxId = correlatorid[0];
            } else {
                outboxId = correlator;
            }

            responseData.put("requestId", jsonData.getString("requestIdentifier"));

            responseData.put("queueType", "CONTENT");
            //responseData.put("text", jsonData.getString("text"));
            responseData.put("network", "SAFARICOM");
            responseData.put("shortCode", jsonData.getString("shortcode"));
            responseData.put("outboxId", outboxId);
            responseData.put("refNo", jsonData.getString("correlator"));
            responseData.put("correlator", jsonData.getString("correlator"));
            responseData.put("sdpServiceId", jsonData.getString("serviceId"));
            responseData.put("sdpId", jsonData.getString("spid"));
            responseData.put("status_code", responseData.getString("status"));

        } catch (Exception e) {
            logger.error("Problbem construction request dlr request:", e);
            this.reject();
            return -1;
        }

        int updateDb = updateDb(responseData);

        logger.info("DB update RESULT:" + updateDb);

        if (updateDb < 0) {
            logger.info("FAILED DB UPDATE RETURNING TO Q: " + updateDb);
            this.reject();
            return -1;
        }

        if (String.valueOf(responseData.getString("status")).equalsIgnoreCase("DeliveredToNetwork")) {
            logger.info("Message status DeliveredToNetwork Re-queueing");
            this.republishMessage(jsonData);
        }

        this.ack();
        return 1;
    }

//    public static void main(String args[]) {
//        JSONObject json = new JSONObject();
//        json.put("data", "Ya");
//        json.put("requestId", "Ya");
//        republishMessage(json);
//    }
    public void republishMessage(JSONObject data) {

        //Props props = new Props();
        //Logging logger = new Logging(props);
        System.out.println("Props set OK ...");
        MessageQueueEndPoint msQ
                = new MessageQueueEndPoint(logger, props,
                        props.getConsumerQueueNames()[0],
                        props.getConsumerExchange()[0],
                        props.getConsumerQueueTypes()[0],
                        Boolean.valueOf(props.getConsumerDurability()[0]),
                        Boolean.valueOf(props.getConsumerExchangeDurability()[0]),
                        props.getRoutingKeys()[0]);
        Channel _channel = null;
        System.out.println("Constructing message ...");
        AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties.Builder();

        try {
            logger.info("Trying to get channel  ..." + data.getString("requestIdentifier"));
            _channel = msQ.getNewChannel();
            logger.info("Found channel  ...");
            _channel.txSelect();
            System.out.println("Strating trx ..." + data.getString("requestIdentifier"));
            _channel.basicPublish(props.getConsumerExchange()[0],
                    props.getRoutingKeys()[0],
                    builder.priority(1).contentType("text/plain").deliveryMode(2).build(),
                    data.toString().getBytes());
            logger.info("Basic publish ..." + data.getString("requestIdentifier"));
            _channel.txCommit();
            logger.info("Commit OK ..." + data.getString("requestIdentifier"));

            logger.info("Message re-queued success proceeding   ...:" + data.toString());
            _channel.close();

        } catch (JSONException | IOException | TimeoutException ex) {
            logger.error("Unable to publish message  ... aborting", ex);
        }
    }

    public int updateDb(JSONObject requestData) {
        logger.info("Callin updatDB ...");
        long tStart = System.currentTimeMillis();
        Connection conn = null;
        Statement stmt = null;
        int result[] = new int[0];

        MySQL connectionPool = null;
        String query = "";
        String alertUpdateQuery = null;

        String service_id_shorcode = requestData.getString("sdpServiceId") + "_" + requestData.getString("shortCode");
        logger.info("Calling  service_id_shorcode..." + service_id_shorcode);

        String pool = props.getMysqlPoolName(service_id_shorcode);

        logger.info("Callin  service_id_shorcode..." + String.valueOf(pool));

        String outboxId = requestData.getString("outboxId");
        String alertId = outboxId;
        String status = requestData.getString("status_code");

        if (pool != null) {
            if (pool.equals("emali5")) {
                connectionPool = emali5pool;
                query = "update outbox set status = '" + status + "', number_of_sends = (number_of_sends +1), "
                        + " sends_required = 0  "
                        + " where outbox_id = '" + outboxId + "' limit 1";

                if (status.equalsIgnoreCase("DeliveredToTerminal")) {
                    alertUpdateQuery = " UPDATE alert a JOIN outbox o ON o.alert_id = a.alert_id  "
                            + " SET a.delivered = (a.delivered+1) where o.outbox_id = '" + outboxId + "'";
                }

            }

        }
        if (connectionPool == null) {
            alertId = outboxId;
            connectionPool = emali3pool;
            String localStatus = "UNDELIVERED";
            switch (status) {
                case "DeliveredToTerminal":
                    localStatus = "DELIVERED";
                    break;
                case "UserNotSubscribed":
                    localStatus = "REJECTED";
                    break;
            }
            query = "update outboxes set status = '" + localStatus + "', sdp_status = '" + status + "', "
                    + " number_of_sends = number_of_sends +1, sends_required = 0  "
                    + " where alerts_id = '" + alertId + "' and msisdn = '" + requestData.getString("msisdn") + "'";

            if (status.equals("DeliveredToTerminal")) {
                alertUpdateQuery = " update alerts set alerts_delivered_count = alerts_delivered_count+ 1 where alerts_id = '" + alertId + "'";
            }
        }

        logger.info("Running update QUERIES: " + query + " : " + alertUpdateQuery);
        try {
            conn = connectionPool.getConnection();
            stmt = conn.createStatement();
            stmt.addBatch(query);
            if (alertUpdateQuery != null) {
                stmt.addBatch(alertUpdateQuery);
            }
            result = stmt.executeBatch();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            logger.error(ProcessQueueMessages.class
                    .getName()
                    + " | updateEmaliMaster --- SQL "
                    + "\n Query => " + query + " \n Message => " + ex.getMessage());

        } finally {

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(ProcessQueueMessages.class.getName()
                            + " | postRequisitions --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(ProcessQueueMessages.class.getName()
                            + " | postRequisitions --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
        }
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        double elapsedSeconds = tDelta / 1000.0;
        logger.info(" : DB UPDATE took +" + elapsedSeconds);
        return result.length > 0 ? 1 : -1;

    }

    @Override
    public void run() {

        logger.info("Calling process DLR  shortcode for POLL");
        try {

            processDlr(jsonData);
        } catch (JSONException e) {
            logger.error("Error calling process DLR ..", e);
            this.ack();
        } catch (Exception ex) {
            logger.error("Error calling process DLR ..", ex);
            this.ack();
        }

        //testRequeue();
    }
}
