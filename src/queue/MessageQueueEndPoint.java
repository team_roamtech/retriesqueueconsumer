/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

//import queue.QMessage;
import com.rabbitmq.client.AMQP;
import utils.Logging;

import java.io.IOException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;
import utils.Props;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author rube
 */
public class MessageQueueEndPoint {

    private transient static Connection connection = null;
    public transient String endPointName;
    protected transient static Logging logger;
    protected transient static Props props;
    protected transient String exchange;
    protected transient String queueType;
    protected transient boolean queueDurability;
    protected transient boolean exchangeDurability;
    transient String routingKey;
    protected final XStream xStream;

    protected final String EXCHANGE_SUFFIX = "";
    private final ThreadLocal<Channel> channels = new ThreadLocal<Channel>();
    final AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties.Builder();

    /**
     * Declare message end point and queue on status
     *
     *
     * @param logging
     * @param properties
     * @param queueName
     * @param exchange
     * @param queueType
     * @param queueDurability
     * @param exchangeDurability
     * @param routingKey
     */
    public MessageQueueEndPoint(Logging logging, Props properties, String queueName,
            String exchange, String queueType, boolean queueDurability,
            boolean exchangeDurability, String routingKey) {

        if (queueName != null) {
            this.setQueueName(queueName);
        }
        logger = logging;
        logger.info("Initializint MessageQueueEndPoint ... ");
        props = properties;
        if (exchange != null) {
            this.exchange = exchange;
        }
        if (queueType != null) {
            this.queueType = queueType;
        }

        this.queueDurability = queueDurability;
        this.exchangeDurability = exchangeDurability;
        this.routingKey = routingKey;
        this.xStream = new XStream(new JettisonMappedXmlDriver());
        try {
            //getting a connection only create a connection when non is present
            logger.info("MessageQueueEndPoint attempting to create connection ..");
            connection = getRabbitConnection();
            logger.info("MessageQueueEndPoint connection established ..");
        } catch (Exception e) {
            logger.error("Error connecting to MQ Server." + e.getMessage());

        }
    }

    /**
     * Closes the Queue Connection. This is not needed to be called explicitly
     * as connection closure happens implicitly anyways.
     *
     */
    public void close() {
        try {
            if (connection != null) {
                {
                    if (connection.isOpen()) {
                        connection.close();
                    }
                }
            }
            connection = null;
        } catch (IOException ioe) {
            connection = null;
            logger.error("Error closing connection:", ioe);
        } catch (Exception e) {
            connection = null;
            logger.error("General exceprion closing connection", e);
        }

    }

    public int getCurrentMessageCount() throws IOException {
        return getChannel().queueDeclarePassive(endPointName).getMessageCount();
    }

    /**
     * Maintain and Return Thread specific channel objects
     *
     * @return
     * @throws IOException
     */
    public final Channel getChannel() throws IOException {

        Channel channel = channels.get();
        if (channel == null || !channel.isOpen()) {
            connection = getRabbitConnection();

            channel = connection.createChannel();
            channel.addShutdownListener(shutdownListener);
            //We remove confirmListeners untill we are able to handle async response - mean while we do transactions
            //channel.addConfirmListener(confirmListener);
            //channel.confirmSelect();
            //declaring a queue for this channel. If queue does not exist, it will be created on the server.
            //durability (second param) is also set as TRUE (the queue will survive a server restart)
            logger.info("Declaring end queue endPoint :" + endPointName + ", exchange :" + exchange);
            channel.queueDeclare(endPointName, true, queueDurability, false, null);
            channel.exchangeDeclare(exchange, queueType, true, exchangeDurability, null);
            channel.queueBind(endPointName, exchange, routingKey);
            channels.set(channel);
            logger.info("Channels set" + endPointName + ", exchange :" + exchange);
        }
        return channel;
    }

    public final Channel getNewChannel() throws IOException {

        connection = getRabbitConnection();

        Channel channel = connection.createChannel();
        channel.addShutdownListener(shutdownListener);
        //We remove confirmListeners untill we are able to handle async response - mean while we do transactions
        //channel.addConfirmListener(confirmListener);
        //channel.confirmSelect();
        //declaring a queue for this channel. If queue does not exist, it will be created on the server.
        //durability (second param) is also set as TRUE (the queue will survive a server restart)
        logger.info("Declaring end queue endPoint :" + endPointName + ", exchange :" + exchange);
        System.err.println("Declaring end queue endPoint :" + endPointName + ", exchange :"
                + exchange + " Durability " + queueDurability + "Route " + routingKey + "Qtype " + queueType);
        channel.queueDeclare(endPointName, true, queueDurability, false, null);
        System.err.println("Queue declare OK");
        channel.exchangeDeclare(exchange, queueType, true, exchangeDurability, null);
        System.err.println("Exchnage declare ok");
        channel.queueBind(endPointName, exchange, routingKey);

        System.err.println("Queue Bind OK: " + channel);

        return channel;

    }

    /**
     * Maintain and Return Thread specific channel objects
     *
     * @param alertType
     * @return
     * @throws IOException
     */
    protected final Channel getChannel(String alertType) throws IOException {
        //setEndPointName(alertType);
        //setQueueName(alertType);
        String currentQueueName, currentExchanegName;

        currentQueueName = endPointName;
        currentExchanegName = exchange;

        if (connection == null) {
            logger.debug("Connection null, restablishing rabbit connection");
            connection = getRabbitConnection();
        } else if (!connection.isOpen()) {
            logger.debug("Found connection not open attempting to open new connection ...");
            connection = getRabbitConnection();
        }

        if (connection == null) {
            logger.fatal("Faield to find connection aborting ... ");
            return null;
        } else {
            logger.info("Rabbit connection established ... " + connection);
        }

        Channel channel = connection.createChannel();
        channel.addShutdownListener(shutdownListener);
        //We remove confirmListeners untill we are able to handle async response - mean while we do transactions
        //channel.addConfirmListener(confirmListener);
        //channel.confirmSelect();
        //declaring a queue for this channel. If queue does not exist, it will be created on the server.
        //durability (second param) is also set as TRUE (the queue will survive a server restart)
        channel.queueDeclare(currentQueueName, true, false, false, null);
        channel.exchangeDeclare(currentExchanegName, "direct", true, false, null);
        channel.queueBind(currentQueueName, currentExchanegName, currentQueueName);

        return channel;
    }

    protected ShutdownListener shutdownListener = new ShutdownListener() {
        @Override
        public void shutdownCompleted(ShutdownSignalException sig) {
            logger.error("ShutdownSignal: reason: "
                    + sig.getReason() + " \nReference: " + sig.getReason()
                    + " Standard QueInitialize shutdown");

        }
    };

    protected ConfirmListener confirmListener = new ConfirmListener() {
        @Override
        public void handleAck(long seqNo, boolean multiple) {
            logger.info("Reading confirm listener: " + seqNo + ", " + multiple);
        }

        @Override
        public void handleNack(long seqNo, boolean multiple) {
            logger.info("Reading confirm listener: " + seqNo + ", " + multiple);
        }
    };

    /**
     *
     * @return @throws IOException
     */
    private Connection getRabbitConnection() throws IOException {
        if (connection != null) {
            if (connection.isOpen()) {
                return connection;
            } else {
                close();
            }
        }

        com.rabbitmq.client.ConnectionFactory factory;

        try {
            factory = new com.rabbitmq.client.ConnectionFactory();
            factory.setRequestedHeartbeat(45);
            factory.setConnectionTimeout(5000);
            factory.setAutomaticRecoveryEnabled(true);
            factory.setTopologyRecoveryEnabled(true);

            System.out.println(" Rabbit Host " + props.getRabbitHost());
            factory.setHost(props.getRabbitHost());
            factory.setVirtualHost(props.getRabbitVhost());
            factory.setUsername(props.getRabbitUsername());
            factory.setPassword(props.getRabbitPassword());
            factory.setPort(Integer.parseInt(props.getRabbitPort()));
            factory.setRequestedHeartbeat(45);

            // Create a new connection to MQ
            connection = factory.newConnection();
        } catch (IOException ex) {
            logger.error("Exception getRabbitConnection creating connection ", ex);
        } catch (TimeoutException ex) {
            logger.error("TimeoutException getRabbitConnection creating connection ", ex);
        }

        return connection;
    }

    /**
     * @param endPointName the endPointName to set
     */
    public void setEndPointName(String endPointName) {
        this.endPointName = endPointName;
    }

    private void setQueueName(String name) {
        this.setEndPointName(name);
    }

    /**
     * @param exchange the exchange to set
     */
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

}
