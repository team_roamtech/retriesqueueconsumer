package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONObject;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import utils.Logging;
import utils.Props;

/**
 * Created by dennis on 2/2/16.
 */
public class RestClient {

    public static final String TAG = "RestClient";
    static String data;
    public static final int GET = 0;
    public static final int POST = 1;
    protected static Logging logger;

    public RestClient() {

    }

    public RestClient(Logging logging) {
        logger = logging;
    }

    public static void main(String[] args) {
        String spPasswordString = "";
        String spID = "601384";
        String spPassword = "Vcy!#5863";

        Date today = new Date();
        long timestamp = today.getTime();

        System.err.println("Received PARAMS :SPID " + spID + ", spPASS :" + spPassword + ", Timestamp:" + timestamp);

        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today);

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            System.err.println("NOW: " + now);
            spPasswordString = createSpPass(spID, now, md, spPassword);
            System.err.println("SPS PASS: " + spPasswordString);
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e.getLocalizedMessage());
        } catch (Exception ex) {
            System.err.println(ex.getLocalizedMessage());
        }

    }

    /**
     * @param msisdn
     * @param serviceId
     * @param requestID
     * @param spID
     * @param spPassword
     * @param sdpURL
     * @return Sample soap message looks like this
     * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
     * xmlns:v2="http://www.huawei.com.cn/schema/common/v2_1"
     * xmlns:loc="http://www.csapi.org/schema/parlayx/sms/send/v2_2/local">
     * <soapenv:Header>
     * <v2:RequestSOAPHeader>
     * <v2:spId>35000001</v2:spId>
     * <v2:spPassword>e6434ef249df55c7a21a0b45758a39bb</v2:spPassword>
     * <v2:serviceId>35000001000001</v2:serviceId>
     * <v2:timeStamp>20120812005752</v2:timeStamp>
     * <v2:OA>tel:254722123456</v2:OA>
     * <v2:FA>tel:254722123456</v2:FA>
     * </v2:RequestSOAPHeader>
     * </soapenv:Header>
     * <soapenv:Body>
     * <loc:getSmsDeliveryStatus>
     * <loc:requestIdentifier>100005200401110225063201000041</loc:requestIdentifier>
     * </loc:getSmsDeliveryStatus>
     * </soapenv:Body>
     * </soapenv:Envelope>
     */
    public String getSMSDeliveryStatus(String msisdn, String serviceId,
            String shortcode, String requestID, String spID, String spPassword, String sdpURL) {

        Date today = new Date();
        logger.info("Received PARAMS :SPID " + spID + ", spPASS :" + spPassword);

        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today);
        String spPasswordString = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            spPasswordString = createSpPass(spID, now, md, spPassword);
        } catch (NoSuchAlgorithmException e) {
            logger.error("Failed to generate SP Password", e);
            return null;
        } catch (Exception ex) {
            logger.error("General exception generating sp pass string ", ex);
            return null;
        }

        String rawSoap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                + "xmlns:v2=\"http://www.huawei.com.cn/schema/common/v2_1\""
                + "xmlns:loc=\"http://www.csapi.org/schema/parlayx/sms/send/v2_2/local\">"
                + "<soapenv:Header>"
                + "<v2:RequestSOAPHeader>"
                + "<v2:spId>" + spID + "</v2:spId>"
                + "<v2:spPassword>" + spPasswordString + "</v2:spPassword>"
                + "<v2:serviceId>" + serviceId + "</v2:serviceId>"
                + "<v2:timeStamp>" + now + "</v2:timeStamp>"
                + "<v2:OA>tel:" + msisdn + "</v2:OA>"
                + "<v2:FA>tel:" + msisdn + "</v2:FA>"
                + "</v2:RequestSOAPHeader>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<loc:getSmsDeliveryStatus>"
                + "<loc:requestIdentifier>" + requestID + "</loc:requestIdentifier>"
                + "</loc:getSmsDeliveryStatus>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        HttpURLConnection conn = null;
        String soapURL = sdpURL;
        InputStream is = null;
        logger.info("PREPARED XML REQ IS " + rawSoap);
        try {
            URL url = new URL(soapURL);
            System.out.println("URL IS " + url);
            logger.info("URL IS " + url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/soap+xml; charset=utf-8");
            conn.setRequestProperty("Accept", "text/xml");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            //Writing dataToSend to outputstreamwriter
            writer.write(rawSoap);
            //Sending the data to the server - This much is enough to send data to server
            writer.flush();
            int responseCode = conn.getResponseCode();
            logger.info("RESPONSE FROM SDP is " + String.valueOf(responseCode));

            if (responseCode == 200) {
                // Read the response
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String xml;
                String responseXML = "";
                while ((xml = bufferedReader.readLine()) != null) {
                    responseXML += xml;
                }
                logger.info("RETURNING XML:" + responseXML);
                return responseXML;
            } else {
                logger.info("SDP CALL STATUS NOT 200 returning null ");
                String responseXML = readIt(conn.getErrorStream());
                logger.info("RETURNING FAILED XML:" + responseXML);
                String faultcode = getResponseErrorStatus(responseXML);

                //POLICY ERROR - Message status is overdue.
                if (faultcode.equalsIgnoreCase("POL0903")) {
                    return "Overdue";
                }
                return null;

            }
        } catch (IOException e) {
            if (conn != null) {
                is = conn.getErrorStream();
            }
            logger.error("Failed response is " + readIt(is), e);
            return null;
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (IOException e) {
                logger.error("Failed Closing Input Stream " + readIt(is), e);
                conn = null;
                is = null;
            }
        }

    }

    /**
     *
     * @param xmlString
     * @return
     * <?xml version="1.0" encoding="utf-8" ?>
     * <env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><env:Body>
     * <ns1:Fault xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/">
     * <faultcode>POL0903</faultcode><faultstring>Message status is
     * overdue.</faultstring>
     * <detail><ns2:PolicyException xmlns:ns2="http://www.csapi.org/schema/parlayx/common/v2_1">
     * <messageId>POL0903</messageId><text>Message status is overdue.</text>
     * <variables>System
     * self</variables><variables>36000</variables><variables>66427</variables>
     * </ns2:PolicyException></detail></ns1:Fault></env:Body></env:Envelope>
     */
    public static String getResponseErrorStatus(String xmlString) {

        JSONObject json = new JSONObject();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlString));
            try {
                org.w3c.dom.Document doc = db.parse(is);
                NodeList nodeList = doc.getElementsByTagName("ns1:Fault");
                if (nodeList == null) {
                    logger.info("Not fault code Error lis Problem parsing xml: ns1:Fault");
                    return null;
                }
                NodeList cn = nodeList.item(0).getChildNodes();
                if (cn == null) {
                    logger.info("Null faultcode childred Problem parsing xml: ns1:Fault");
                    return null;
                }
                String faultcode = cn.item(0).getTextContent();

                logger.info("Extractin FAULT from SOAP RESPONSE:" + faultcode);
                return faultcode;

            } catch (SAXException e) {
                // handle SAXException
                logger.error("SAXException Problem parsing xml", e);
            } catch (IOException e) {
                // handle IOException
                logger.error("IOException Problem parsing xml", e);
            }
        } catch (ParserConfigurationException e1) {
            logger.error("ParserConfigurationException Problem parsing xml", e1);

            // handle ParserConfigurationException
        }
        return null;

    }

    /**
     * @param xmlString
     *
     * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     * <soapenv:Body>
     * <ns1:getSmsDeliveryStatusResponse
     * xmlns:ns1="http://www.csapi.org/schema/parlayx/sms/send/v2_2/local">
     * <ns1:result>
     * <address>tel:254722123456</address>
     * <deliveryStatus>DeliveredToTerminal</deliveryStatus>
     * </ns1:result>
     * </ns1:getSmsDeliveryStatusResponse>
     * </soapenv:Body>
     * </soapenv:Envelope>
     * @return
     */
    public static JSONObject parserDLRResponse(String xmlString) {
        logger.info("Calling parse XML Request: " + xmlString);

        JSONObject json = new JSONObject();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlString));

            try {
                logger.info("Try extract the data from XML");
                org.w3c.dom.Document doc = db.parse(is);
                NodeList nodeList = doc.getElementsByTagName("ns1:result");
                logger.info("NS1 Result node is :" + String.valueOf(nodeList));
                if (nodeList == null) {
                    logger.info("Null node list Problem parsing xml: ns1:result");
                    return null;
                }
                if (nodeList.item(0) == null) {
                    logger.info("NOlelist: ns1:result,  has no children");
                    return null;
                }
                NodeList cn = nodeList.item(0).getChildNodes();

                logger.info("ChildNode Result node is :" + String.valueOf(cn));

                if (cn == null) {
                    logger.info("Null not list Problem parsing xml: ns1:result");
                    return null;
                }

                if (cn.item(0) == null) {
                    logger.info("CN Nodelist (2) has no childres ns1:result");
                    return null;
                }

                String address = cn.item(0).getTextContent();
                String deliveryStatus = cn.item(1).getTextContent();

                logger.info("Extractin DATA from SOAP RESPONSE:" + address + ", " + deliveryStatus);

                json.put("msisdn", address.replace("tel:", ""));
                json.put("status", deliveryStatus);

                logger.info("Returning JSON RESPONSE: " + address + ", " + deliveryStatus);
                return json;

            } catch (SAXException e) {
                // handle SAXException
                logger.error("SAXException Problem parsing xml", e);
            } catch (IOException e) {
                // handle IOException
                logger.error("IOException Problem parsing xml", e);
            } catch (Exception e) {
                logger.error("Exception parsing XML", e);
            }
        } catch (ParserConfigurationException e1) {
            logger.error("ParserConfigurationException Problem parsing xml", e1);

            // handle ParserConfigurationException
        }
        return null;

    }

    private static String createSpPass(String spIdString, String now, MessageDigest md, String pipe_pass) {
        String allpass = spIdString + pipe_pass + now;
        md.reset();
        md.update(allpass.getBytes());
        byte[] digest = md.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String spPasswordString = bigInt.toString(16);
        while (spPasswordString.length() < 32) {
            spPasswordString = "0" + spPasswordString;
        }
        return spPasswordString;
    }

    public int makeRequest(int method, String sdpURL, String json) throws Exception {
        InputStream is = null;

        logger.info("POSTING DATA " + sdpURL + ", " + json);
        if (method == POST) {
            HttpURLConnection conn = null;
            try {
                URL url = new URL(sdpURL);
                System.out.println("URL IS " + url);
                logger.info("URL IS " + url);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                //Writing dataToSend to outputstreamwriter
                writer.write(json);

                //Sending the data to the server - This much is enough to send data to server
                writer.flush();
                int response = conn.getResponseCode();
                System.out.println("response is " + String.valueOf(response));
                return response;
            } catch (IOException e) {
                if (conn != null) {
                    is = conn.getErrorStream();
                }
                logger.error("Failed response is " + readIt(is), e);
                return 0;
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                    if (conn != null) {
                        conn.disconnect();
                    }
                } catch (IOException e) {
                    logger.error("Failed Closing Input Stream " + readIt(is), e);
                    conn = null;
                    is = null;
                }
            }
        }
        return 0;
    }

    public String readIt(InputStream is) {
        StringBuilder sb = null;
        if (is != null) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                is.close();
            } catch (UnsupportedEncodingException e) {
                logger.error("Failed Reading response from server " + e.getMessage(), e);
                System.err.println("Failed Reading response from server " + e);
            } catch (IOException e) {
                logger.error("Failed Reading response from server " + e.getMessage(), e);
                System.err.println("Failed Reading response from server " + e);
            }
            return sb.toString();
        }
        return "error reading Input Stream";
    }
}
