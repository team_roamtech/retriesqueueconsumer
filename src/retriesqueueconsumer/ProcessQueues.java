/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retriesqueueconsumer;

import db.MySQL;
import java.io.IOException;
import queue.Consumer;
import utils.Logging;
import utils.Props;

/**
 *
 * @author dennis
 */
public class ProcessQueues implements Runnable {

    Logging logger;
    Props props;
    String queueName, queueExchange, queueType, url;
    boolean queueDurability, exchangeDurability;
    transient String routingKey;
    private final MySQL emali5pool, emali3Pool;

    public ProcessQueues(Logging logger, Props props, String queueName, String queueExchange,
            String queueType, boolean queueDurability,
            boolean exchangeDurability, String url, String route, MySQL emali5pool, MySQL emali3pool) {
        this.logger = logger;
        this.props = props;
        this.queueName = queueName;
        this.queueExchange = queueExchange;
        this.queueType = queueType;
        this.url = url;
        this.queueDurability = queueDurability;
        this.exchangeDurability = exchangeDurability;
        this.routingKey = route;
        this.emali5pool = emali5pool;
        this.emali3Pool = emali3pool;

    }

    @Override
    public void run() {
        try {
            Consumer consumer = Consumer.getInstance(logger, props, queueName,
                    queueExchange, queueType, queueDurability, exchangeDurability,
                    url, routingKey, emali5pool, emali3Pool);
            consumer.consume();
        } catch (IOException ex) {
            logger.error("Consumer failed " + queueName, ex);
        }
    }

}
