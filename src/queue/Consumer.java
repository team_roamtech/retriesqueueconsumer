/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import db.MySQL;
import utils.Logging;
import utils.Props;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;
import retriesqueueconsumer.ProcessQueueMessages;

/**
 *
 * @author rube
 */
public class Consumer extends MessageQueueEndPoint {

    transient ProcessQueueMessages processQueueMessage;
    public transient ExecutorService executor;
    private final MySQL emali3pool, emali5pool;
    //MessageQueueEndPoint messageQueueEndPoint = null;

    public Consumer(Logging logger, Props props,
            String queueName, String queueExchange, String queueType, boolean queueDurability,
            boolean exchangeDurability, String url, String route, MySQL emali5pool, MySQL emali3pool) throws IOException {
        super(logger, props, queueName, queueExchange, queueType, queueDurability, exchangeDurability, route);
        logger.info("Consumer:: Creating consumer ");
        executor = Executors.newFixedThreadPool(props.getNumOfThreads());
        this.emali3pool = emali3pool;
        this.emali5pool = emali5pool;

    }

    public void consume() {
        logger.info("Calling this.channel.basicConsume ...starting success");
        final Channel channel;
        final QueueingConsumer consumer;

        try {
            channel = getChannel();
            consumer = new QueueingConsumer(channel);
            channel.basicQos(100);
            System.err.println(" EndPointName " + endPointName + " Exchange " + exchange);
            channel.basicConsume(endPointName, false, consumer);
        } catch (IOException e) {
            logger.info("IOException attemting trying acquire channel .." + e.getLocalizedMessage());
            this.close();
            return;

        }
        int activeThread = 0;

        while (true) {
            logger.info(" Waiting for messages .... from: " + consumer.getChannel());
            try {
                final QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                logger.info("Delivery received .... proceeding to process");
                if (activeThread >= props.getBucketSize()) {
                    //shutdown and restart consumer
                    shutdownAndAwaitTermination(executor);
                }
                //We recreate this guy after every so threads on success shutdown
                if (executor == null || executor.isShutdown() || executor.isTerminated()) {
                    logger.info(" Resetting executore service on  .... threshhold: " + activeThread);
                    executor = Executors.newFixedThreadPool(props.getNumOfThreads());
                    activeThread = 0;
                }
                handleDelivery(delivery, channel);
                activeThread++;
            } catch (ConsumerCancelledException cce) {
                logger.error("ConsumerCancelledException trying to process message : ", cce);
                this.close();
                retriesqueueconsumer.RetriesQueueConsumer.isCurrentPoolShutDown = true;
                break;
            } catch (InterruptedException ie) {
                logger.error("InterruptedException trying to process message : ", ie);
                this.close();
                retriesqueueconsumer.RetriesQueueConsumer.isCurrentPoolShutDown = true;
                break;
            } catch (ShutdownSignalException ie) {
                logger.error("ShutdownSignalException trying to process message : ", ie);
                this.close();
                retriesqueueconsumer.RetriesQueueConsumer.isCurrentPoolShutDown = true;
                break;
            } catch (Exception ex) {
                logger.error("General Exception trying to process message : ", ex);
                this.close();
                retriesqueueconsumer.RetriesQueueConsumer.isCurrentPoolShutDown = true;
                break;
            }

        }
        logger.info("Broke out of consumption loop will attempt to reconnect ..");
    }

    public static Consumer getInstance(Logging logging, Props props,
            String queueName, String queueExchange, String queueType, boolean queueDurability,
            boolean exchangeDurability, String url, String route, MySQL emali5pool, MySQL emali3pool) throws IOException {
        Consumer consumer = new Consumer(logging, props, queueName, queueExchange,
                queueType, queueDurability, exchangeDurability, url, route, emali5pool, emali3pool);
        return consumer;
    }

    /**
     *
     * @param delivery
     * @param channel Message looks like this
     * {"requestIdentifier":"100002200301160607151739913011","msisdn":"254722904244",
     * "serviceId":"6015132000112536","shortcode":"60040","correlator":"14341_254722904244",
     * "password":"F1vu#Wj4h","spid":"601513"}
     */
    public void handleDelivery(QueueingConsumer.Delivery delivery, Channel channel) {
        Envelope envelope = delivery.getEnvelope();

        String message = new String(delivery.getBody());
        logger.info("Received Message:" + message);
        logger.info("Creating new task for message received ");

        try {
            JSONObject jObject = new JSONObject(message);
            if (jObject instanceof JSONObject) {

                logger.info("Starting new TASK with message:" + jObject.toString());

                processQueueMessage = new ProcessQueueMessages(logger,
                        jObject, channel, envelope.getDeliveryTag(), props,
                        emali5pool, emali3pool);
                executor.submit(processQueueMessage);
            } else {
                logger.error("Invalid Request from the Queue: " + message);
                try {
                    channel.basicAck(envelope.getDeliveryTag(), false);
                } catch (IOException exception) {
                    logger.error("Consume message failed basic Reject :" + exception.getLocalizedMessage(), exception);
                }
            }
        } catch (JSONException ex) {
            logger.error("JSONException thrown " + ex.getMessage(), ex);
            try {
                logger.info("Calling message basic accept after Invalid message: IOException");
                channel.basicAck(envelope.getDeliveryTag(), false);
            } catch (IOException exception) {
                logger.error("Consume message failed basic ack :" + exception.getLocalizedMessage(), exception);
            }

        } catch (Exception e) {
            logger.error("Error processing request: " + e.getLocalizedMessage(), e);
            try {
                logger.info("Calling message basic reject after Exception");
                channel.basicReject(envelope.getDeliveryTag(), true);
            } catch (IOException exception) {
                logger.error("Consume message failed basic Reject :" + exception.getLocalizedMessage(), exception);
            }
        }
    }

    /**
     * The following method shuts down an ExecutorService in two phases, first
     * by calling shutdown to reject incoming tasks, and then calling
     * shutdownNow, if necessary, to cancel any lingering tasks (after 6
     * minutes).
     *
     * @param pool the executor service pool
     */
    public static void shutdownAndAwaitTermination(final ExecutorService pool) {
        logger.info("Executor pool  waiting for tasks to complete");
        pool.shutdown(); // Disable new tasrks from being submitted

        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(30, TimeUnit.SECONDS)) {
                logger.error("Executor pool  terminated with "
                        + "tasks unfinished. Resetting unfinished tasks");
                pool.shutdownNow(); // Cancel currently executing tasks

                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(5, TimeUnit.SECONDS)) {
                    logger.error("Executor pool  terminated "
                            + "with tasks unfinished. Resetting unfinished "
                            + "tasks");

                }
            } else {
                logger.info("Executor pool  completed all "
                        + "tasks and has shut down");
            }
        } catch (InterruptedException ie) {
            logger.error("Executor pool ' shutdown error: "
                    + ie.getMessage());
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }

    }

}
