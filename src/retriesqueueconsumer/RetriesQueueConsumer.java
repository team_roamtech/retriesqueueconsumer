/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retriesqueueconsumer;

import db.MySQL;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import utils.Constants;
import utils.Logging;
import utils.Props;

/**
 *
 * @author dennis
 */
public class RetriesQueueConsumer {

    /**
     * Logger for this application.
     */
    private static Logging log;
    /**
     * Loads system properties.
     */
    private static Props props;

    /**
     * The main processRequests class.
     */
    private static transient ProcessQueues processQueues;

    public static boolean isCurrentPoolShutDown = false;
    private static MySQL emali5pool;
    private static MySQL emali3pool;

    public RetriesQueueConsumer(Props properties, Logging logger, MySQL emali5pool,
            MySQL emali3pool) {
        props = properties;
        log = logger;
        RetriesQueueConsumer.emali3pool = emali3pool;
        RetriesQueueConsumer.emali5pool = emali5pool;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.setProperty("jsse.enableSNIExtension", "false");
        init();

        System.err.println(props.getSPPassword("601384"));

        while (true) {
            try {
                log.info("starting processRequests ... ");
                fetchAndProcessQueue();
                Thread.sleep(props.getSleepTime());
            } catch (Exception ex) {
                log.error("General error occured: " + ex.getMessage());
            }
        }
    }

    /**
     * Processes messages.
     */
    public void processQueue() {
        int daemonState = Constants.DAEMON_RUNNING;
        log.info("I am in my running state. Running_state id: "
                + Constants.DAEMON_RUNNING);

        int rabbitState = pingServer(props.getRabbitHost(), props.getRabbitPort());

        int emali3State = pingServer(props.getEmal3DbHost(), props.getEmal3DbPort());
        int emali5State = pingServer(props.getDbHost(), props.getDbPort());

        if (rabbitState == Constants.PING_SUCCESS && emali5State == Constants.PING_SUCCESS
                && emali3State == Constants.PING_SUCCESS) {
            // Database is available, so allocate and fetch a bucket
            if (daemonState == Constants.DAEMON_RUNNING) {
                fetchAndProcessQueue();
            } else {
                log.info(RetriesQueueConsumer.class
                        .getName()
                        + "| Connection to the either databases and or rabbit queue was re-established. "
                        + "Restoring service...");

                try {
                    Thread.sleep(props.getSleepTime());
                } catch (InterruptedException ex) {
                    log.error("Interrupted while sleeping: " + ex.getMessage());
                }

                log.info(RetriesQueueConsumer.class.getName()
                        + "| resuming daemon service...");
                daemonState = Constants.DAEMON_RUNNING;

                log.info(RetriesQueueConsumer.class
                        .getName()
                        + "| daemon service resumed successfully, working...");
            }
        } else {
            if (rabbitState != Constants.PING_SUCCESS) {
                log.fatal("The MQ server: " + props.getRabbitHost()
                        + " servicing on port: " + props.getRabbitPort()
                        + " appears to be down (internal function for "
                        + "pingDatabaseServer() returned a PING_FAILED status)");
                daemonState = Constants.DAEMON_INTERRUPTED;

                log.info(RetriesQueueConsumer.class.getName()
                        + "| Connection to the MQ was interrupted, "
                        + " suspending service...");
            }
            if (emali3State != Constants.PING_SUCCESS) {
                log.fatal("The Emali3DB server: " + props.getEmal3DbHost()
                        + " servicing on port: " + props.getEmal3DbPort()
                        + " appears to be down (internal function for "
                        + "pingDatabaseServer() returned a PING_FAILED status)");
                daemonState = Constants.DAEMON_INTERRUPTED;

                log.info(RetriesQueueConsumer.class.getName()
                        + "| Connection to the MQ was interrupted, "
                        + " suspending service...");
            }

            if (emali5State != Constants.PING_SUCCESS) {
                log.fatal("The Emali5DB server: " + props.getDbHost()
                        + " servicing on port: " + props.getDbPort()
                        + " appears to be down (internal function for "
                        + "pingDatabaseServer() returned a PING_FAILED status)");
                daemonState = Constants.DAEMON_INTERRUPTED;

                log.info(RetriesQueueConsumer.class.getName()
                        + "| Connection to the MQ was interrupted, "
                        + " suspending service...");
            }

            while (true) {
                // Enter a Suspended state keep trying to connect
                int rstate = pingServer(props.getRabbitHost(), props.getRabbitPort());

                if (rstate == Constants.PING_SUCCESS) {
                    daemonState = Constants.DAEMON_RESUMING;
                    break;
                }
                if (rstate != Constants.PING_SUCCESS) {
                    log.fatal(RetriesQueueConsumer.class.getName()
                            + "| Unable to connect to the MQ . Retrying in 10 "
                            + "seconds..");
                }

                try {
                    Thread.sleep(props.getSleepTime());
                } catch (InterruptedException ex) {
                    log.error("Interrupted while sleeping: " + ex.getMessage());
                }
            }
        }
    }

    public static void fetchAndProcessQueue() {

        String[] queues = props.getConsumerQueueNames();
        String[] queueExchanges = props.getConsumerExchange();
        String[] queueTypes = props.getConsumerQueueTypes();
        String[] queueDurability = props.getConsumerDurability();
        String[] exchangeDurability = props.getConsumerExchange();
        String[] urls = props.getConsumerUrls();
        String[] routes = props.getRoutingKeys();

        ExecutorService consumerExecutor = Executors.newFixedThreadPool(queues.length);
        try {
            //For each queue, create a limited pull of channels to consume messages                
            for (int i = 0; i < queues.length; i++) {
                log.info("Connecting to Q: " + queues[i]);
                System.err.println(queues[i] + " " + queueExchanges[i] + " " + queueTypes[i] + " " + queueDurability[i] + " " + exchangeDurability[i] + " " + urls[i]);
                processQueues = new ProcessQueues(log, props, queues[i],
                        queueExchanges[i], queueTypes[i], Boolean.valueOf(queueDurability[i]),
                        Boolean.valueOf(exchangeDurability[i]),
                        urls[i],
                        routes[i],
                        emali5pool, emali3pool
                );
                consumerExecutor.execute(processQueues);
            }
        } catch (Exception e) {
            shutdownAndAwaitTermination(consumerExecutor);
            log.error("IOException trying to start consumer: " + e.getMessage(), e);
        }
        log.info("Shutting down worker executor");
        consumerExecutor.shutdown();

        while (true) {

            if (isCurrentPoolShutDown) {
                log.info("One of the threads terminated, Forcing total shutdown, System will "
                        + " retart all consumers in a few ...");
                consumerExecutor.shutdownNow();
            }
            if (consumerExecutor.isTerminated()) {
                isCurrentPoolShutDown = false;
                log.info("Main consumer thread is terminated ...");
                break;
            }
            try {
                if (!isCurrentPoolShutDown) {
                    log.info("Waiting infinately for the consumers to finish ...");
                }

                if (!consumerExecutor.awaitTermination(10, TimeUnit.SECONDS)) {
                    try {
                        log.info("Consumer starter going for a nap  ... P: please remember to close the windows");
                        Thread.sleep(props.getSleepTime());
                    } catch (Exception e) {
                        log.info("Someone interupted waiting infinately for the consumers to finish ...");
                    }
                } else {
                    log.info("!consumerExecutor.awaitTermination(1, TimeUnit.MINUTES) returned False");
                }
            } catch (InterruptedException ignored) {
                log.info("Consumer executor InterruptedException shutting down ...");
                // (Re-)Cancel if current thread also interrupted
                consumerExecutor.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
        }
        log.info("Shutdown all consumers .. Terminating call will loop in restart");

    }

    /**
     * The following method shuts down an ExecutorService in two phases, first
     * by calling shutdown to reject incoming tasks, and then calling
     * shutdownNow, if necessary, to cancel any lingering tasks (after 6
     * minutes).
     *
     * @param pool the executor service pool
     */
    public static void shutdownAndAwaitTermination(final ExecutorService pool) {
        log.info("Executor pool  waiting for tasks to complete");
        pool.shutdown(); // Disable new tasrks from being submitted

        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(5, TimeUnit.MINUTES)) {
                log.error("Executor pool  terminated with "
                        + "tasks unfinished. Resetting unfinished tasks");
                pool.shutdownNow(); // Cancel currently executing tasks

                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(1, TimeUnit.MINUTES)) {
                    log.error("Executor pool  terminated "
                            + "with tasks unfinished. Resetting unfinished "
                            + "tasks");

                }
            } else {
                log.info("Executor pool  completed all "
                        + "tasks and has shut down");
            }
        } catch (InterruptedException ie) {
            log.error("Executor pool ' shutdown error: "
                    + ie.getMessage());
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }

    }

    /**
     * Test init().
     */
    public static void init() {
        props = new Props();
        log = new Logging(props);
        log.info("Init starting to read from queue");
    }

    /**
     * Creates a stream socket and connects it to the specified port number on
     * the named host.
     *
     * @param host the host
     * @param port the port
     *
     * @return the state of the ping
     */
    private int pingServer(final String host, final String port) {
        int state;

        try {
            int portNumber = Integer.parseInt(port);
            Socket ps = new Socket(host, portNumber);

            /*
             * Same time we use to sleep is the same time we use for the ping
             * wait period.
             */
            ps.setSoTimeout(props.getSleepTime());

            if (ps.isConnected()) {
                state = Constants.PING_SUCCESS;
            } else {
                state = Constants.PING_FAILED;
            }

            ps.close();
        } catch (UnknownHostException ex) {
            state = Constants.PING_FAILED;
        } catch (SocketException ex) {
            state = Constants.PING_FAILED;
        } catch (IOException ex) {
            state = Constants.PING_FAILED;
        }

        return state;
    }

}
